// Copyright 2019 Ian Denhardt <ian@zenhack.net>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

export interface ReadStore {
	receive(xt: string): PromiseLike<ArrayBuffer>;
}

export interface WriteStore {
	store(value: ArrayBuffer): PromiseLike<string>;
}

export interface RWStore {
	receive(xt: string): PromiseLike<ArrayBuffer>;
	store(value: ArrayBuffer): PromiseLike<string>;
}

export interface DataSrc {
	size(): PromiseLike<number>;
	read(count: number): PromiseLike<ArrayBuffer>;
}

const HASH_ALGORITHMS: string[] = ['sha256d'];
const DEFAULT_ALGO: string = 'sha256d';
export const SHARD_SIZE: number = 32768;

interface Xt {
	algorithm: string;
	digest: string;
}

export class UrnError {
	problem: string;

	constructor(problem: string) {
		this.problem = problem;
	}
}

export class ValidationError {
	problem: string;

	constructor(problem: string) {
		this.problem = problem;
	}
}

// Base64-encode an array buffer.
function bufToBase64(data: ArrayBuffer): string {
	return btoa(String.fromCharCode(...new Uint8Array(data)));
}

function sha256d(crypto: SubtleCrypto, data: ArrayBuffer): PromiseLike<ArrayBuffer> {
	const sha256 = (d: ArrayBuffer) => crypto.digest('SHA256', data);
	return sha256(data).then(sha256);
}

function parseXt(xt: string): Xt {
	const parts = xt.split(':');
	if(parts.length !== 3) {
		throw new UrnError('Invalid length'); // TODO: better message
	}
	const scheme = parts[0];
	const ret = {
		algorithm: parts[1],
		digest: parts[2],
	}

	if(scheme !== 'urn') {
		throw new UrnError('Invalid URI Scheme');
	}
	if(!HASH_ALGORITHMS.includes(ret.algorithm)) {
		throw new UrnError('Unknown hash algorithm');
	}
	return ret;
}

function validateData(data: ArrayBuffer): void {
	if(data.byteLength !== SHARD_SIZE) {
		throw new ValidationError('Invalid shard size')
	}
}

function xtToUrn(xt: Xt): string {
	return 'urn:' + xt.algorithm + ':' + xt.digest;
}

function urnFromDigest(digest: ArrayBuffer): string {
	return xtToUrn({
		algorithm: DEFAULT_ALGO,
		digest: bufToBase64(digest),
	})
}

export class InMemoryStore implements WriteStore {
	constructor(crypto: SubtleCrypto) {
		this._crypto = crypto
		this._items = {};
	}

	_items: { [key: string]: ArrayBuffer };
	_crypto: SubtleCrypto;

	store(value: ArrayBuffer): PromiseLike<string> {
		validateData(value);
		let items = this._items;
		return sha256d(this._crypto, value).then(function(digest) {
			const urn = urnFromDigest(digest);
			items[urn] = value;
			return urn;
		})
	}

	receive(urn: string): PromiseLike<ArrayBuffer> {
		const result = this._items[urn];
		return new Promise((resolve) => resolve(result))
	}
}

export class BufferSrc implements DataSrc {
	buffer: ArrayBuffer;

	constructor(buffer: Buffer) {
		this.buffer = buffer
	}

	size(): number {
		const ret = this.buffer.byteLength;
		return new Promise((r) => r(ret));
	}

	read(count) {
		const result = this.buffer.slice(0, count);
		this.buffer = this.buffer.slice(count, this.buffer.byteLength);
		return new Promise((r) => r(result))
	}
}

export class Shipper {
	_crypto: SubtleCrypto;
	_store: RWStore;

	constructor(crypto: SubtleCrypto, store: RWStore) {
		this._crypto = crypto
		this._store = store
	}

	ship(data: DataSrc): PromiseLike<string> {
		data.size().then(function(sz) {
			const prefix = "(3:raw" + sz.toString() + ":";
			// TODO: finish
		})
	}

	receive(urn: string): PromiseLike<DataSrc> {

	}

	receiveBlock(urn: string): PromiseLike<ArrayBuffer> {

	}
}
